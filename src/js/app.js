import "../scss/app.scss";

window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  const image = document.querySelector(".image");
  image.addEventListener("click", () => {
    image.style.transform = "scale(2)";
    image.style.transition = "transform 0.25s ease";
    image.classList.add("active");
  });
});
